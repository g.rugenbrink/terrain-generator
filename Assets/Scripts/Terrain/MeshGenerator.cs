﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MeshGenerator {
    public static MeshData GenerateTerrainMesh(float[,] heightMap, float heightMultiplier, AnimationCurve meshHeightCurve, int levelOfDetail) {
        int width = heightMap.GetLength(0);
        int height = heightMap.GetLength(1);

        int meshSimplificationIncrement = (levelOfDetail == 0) ? 1 : levelOfDetail * 2;
        int verticesPerLine = (width - 1) / meshSimplificationIncrement + 1;

        MeshData meshData = new MeshData(verticesPerLine, verticesPerLine);

        for (int y = 0, i = 0; y < height; y += meshSimplificationIncrement) {
            for (int x = 0; x < width; x += meshSimplificationIncrement, i++) {
                meshData.vertices[i] = new Vector3(x, meshHeightCurve.Evaluate(heightMap[x, y]) * heightMultiplier, y);
                meshData.uvs[i] = new Vector2(x / (float)width, y / (float)height);

                if(x < width - 1 && y < height - 1) {
                    meshData.AddTriangle(i, i + verticesPerLine + 1, i + verticesPerLine);
                    meshData.AddTriangle(i + verticesPerLine + 1, i, i + 1);
                }
            }
        }

        return meshData;
    }
}

public class MeshData {
    public Vector3[] vertices;
    public int[] triangles;
    public Vector2[] uvs;

    public Mesh mesh;
    public MeshCollider meshCollider;

    int triangleIndex;

    public MeshData(int width, int height) {
        vertices = new Vector3[(width + 1) * (height + 1)];
        uvs = new Vector2[vertices.Length];
        triangles = new int[width * height * 6];
    }

    public void AddTriangle(int a, int b, int c) {
        triangles[triangleIndex] = a;
        triangles[triangleIndex + 1] = c;
        triangles[triangleIndex + 2] = b;
        triangleIndex += 3;
    }

    public Mesh CreateMesh() {
        mesh = new Mesh();
        mesh.name = "Terrain";
        mesh.Clear();
        mesh.vertices = vertices;
        mesh.triangles = triangles;
        mesh.uv = uvs;
        mesh.RecalculateNormals();
        return mesh;
    }
}
