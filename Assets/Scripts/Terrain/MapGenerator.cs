﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class MapGenerator : MonoBehaviour
{
    public enum DrawMode { NOISEMAP, MESH, FALLOFF }

    public DrawMode drawMode;

    public TerrainData terrainData;
    public NoiseData noiseData;
    public TextureData textureData;

    public Material terrainMaterial;

    public int mapChunkSize = 241;
    public static int MapChunkSize { get; set; }
    [Range(0, 6)]
    public int levelOfDetail;
    
    public bool autoUpdate = false;

    public MeshData meshData;

    float[,] fallOffMap;

    private void Awake() {
        textureData.ApplyToMaterial(terrainMaterial);
        textureData.UpdateMeshHeights(terrainMaterial, terrainData.MinHeight, terrainData.MaxHeight);
        MapChunkSize = mapChunkSize;
        DrawMapInEditor();
    }

    private void OnValuesUpdate() {
        if (!Application.isPlaying) {
            DrawMapInEditor();
        }
    }

    private void OnTextureValuesUpdated() {
        textureData.ApplyToMaterial(terrainMaterial);
    } 

    private MapData GenerateMapData() {
        noiseData.seed = (int)DateTimeOffset.Now.ToUnixTimeMilliseconds();
        float[,] noiseMap = Noise.GenerateNoiseMap(mapChunkSize, mapChunkSize,
            noiseData.seed, noiseData.noiseScale, noiseData.octaves, noiseData.persistance, noiseData.lacunarity, noiseData.offset);

        if (terrainData.useFallOff) {
            if (fallOffMap == null) {
                fallOffMap = FallOffGenerator.GenerateFallOffMap(mapChunkSize);
            }

            for (int y = 0; y < mapChunkSize; y++) {
                for (int x = 0; x < mapChunkSize; x++) {
                    noiseMap[x, y] = Mathf.Clamp01(noiseMap[x, y] - fallOffMap[x, y]);
                }
            }
        }

        textureData.UpdateMeshHeights(terrainMaterial, terrainData.MinHeight, terrainData.MaxHeight);

        return new MapData(noiseMap);
    }

    public void DrawMapInEditor() {
        MapData mapData = GenerateMapData();
        MapDisplay mapDisplay = GetComponent<MapDisplay>();

        if (drawMode == DrawMode.NOISEMAP) {
            mapDisplay.DrawTexture(TextureGenerator.TextureFromHeightMap(mapData.heightMap));
        }else if (drawMode == DrawMode.MESH) {
            meshData = MeshGenerator.GenerateTerrainMesh(mapData.heightMap, terrainData.meshHeightMultiplier, terrainData.meshHeightCurve, levelOfDetail);
            mapDisplay.DrawMesh(meshData);
        } else if(drawMode == DrawMode.FALLOFF) {
            mapDisplay.DrawTexture(TextureGenerator.TextureFromHeightMap(FallOffGenerator.GenerateFallOffMap(mapChunkSize)));
        }
    }

    private void OnValidate() {
        if (terrainData != null) {
            terrainData.OnValuesUpdated -= OnValuesUpdate;
            terrainData.OnValuesUpdated += OnValuesUpdate;
        }
        if (noiseData != null) {
            noiseData.OnValuesUpdated -= OnValuesUpdate;
            noiseData.OnValuesUpdated += OnValuesUpdate;
        }
        if(textureData != null) {
            textureData.OnValuesUpdated -= OnTextureValuesUpdated;
            textureData.OnValuesUpdated += OnTextureValuesUpdated;
        }
        MapChunkSize = mapChunkSize;
    }

    [System.Serializable]
    public struct TerrainType {
        public string name;
        public float heightValue;
        public Color colour;
    }

    public struct MapData {
        public float[,] heightMap;

        public MapData(float[,] heightMap) {
            this.heightMap = heightMap;
        }
    }
}
