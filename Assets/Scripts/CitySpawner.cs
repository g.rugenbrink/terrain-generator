﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CitySpawner : MonoBehaviour
{
    public float radius = 1;
    public Vector2 regionSize = Vector2.one;
    public int rejectionSamples = 30;
    
    public ObjectPlacementCheck checkObjectPrefab;

    [Space]
    public float raycastLength = 300f;
    public float minTerrainheight = 2f;
    public float maxTerrainheight = 100f;

    [Space]
    public bool isSpawningCities = true;

    List<Vector2> points;
    public float spawnDelay = 0.1f;

    private void Awake() {
        StartCoroutine(InitSpawns());
    }

    public void OnButtonClick() {
        for (int i = transform.childCount - 1; i >= 0; i--) {
            Destroy(transform.GetChild(i).gameObject);
        }
        StartCoroutine(InitSpawns());
    }

    private IEnumerator InitSpawns() {
        yield return new WaitForSeconds(spawnDelay);
        points = PoissonDiscSampling.GeneratePoints(radius, regionSize, rejectionSamples);
        if (isSpawningCities) {
            SpawnCityLocations();
        } else {
            StartCoroutine(SpawnBuildings());
        }
    }

    private IEnumerator SpawnBuildings() {
        //loops through all the points created with poisson disc sampling
        foreach(Vector2 point in points) {
            //Instantiate an object and sets the position and parent
            var checkObject = Instantiate(checkObjectPrefab);
            checkObject.transform.SetParent(transform);
            checkObject.transform.position = new Vector3(transform.position.x + (point.x - regionSize.x / 2f), 200, transform.position.z + (point.y - regionSize.y / 2f));
            yield return null;
            //Raycasts to check the ground underneath and saves that height
            var terrainheight = checkObject.CheckHeight(raycastLength);
            if (terrainheight >= minTerrainheight && terrainheight <= maxTerrainheight) {
                checkObject.transform.position = new Vector3(checkObject.transform.position.x, terrainheight + (checkObject.transform.localScale.y / 3f), checkObject.transform.position.z);
            }else if (checkObject.transform.position.y == 200) { 
                Destroy(checkObject.gameObject);
            } else {
                Destroy(checkObject.gameObject);
            }
        }
    }

    private void SpawnCityLocations() {
        foreach (Vector2 point in points) {
            var checkObject = Instantiate(checkObjectPrefab);
            checkObject.transform.SetParent(transform);
            checkObject.transform.position = new Vector3((point.x * MapGenerator.MapChunkSize) / 10f, 200, (point.y * MapGenerator.MapChunkSize) / 10f);
            checkObject.transform.rotation = Quaternion.identity;
            var terrainheight = checkObject.CheckHeight(raycastLength);
            if (terrainheight >= minTerrainheight && terrainheight <= maxTerrainheight) {
                checkObject.transform.position = new Vector3(checkObject.transform.position.x, terrainheight + (checkObject.transform.localScale.y / 3f), checkObject.transform.position.z);
            } else if (checkObject.transform.position.y == 200) {
                Destroy(checkObject.gameObject);
            } else {
                Destroy(checkObject.gameObject);
            }
        }
    }
}
