﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPlacementCheck : MonoBehaviour
{
    public float CheckHeight(float raycastLength) {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, Vector3.down, out hit, raycastLength)) {
            return hit.point.y;
        }
        return 0;
    }
}
